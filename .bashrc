#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# paths
export PATH="$HOME/.emacs.d/bin:$PATH"

# prompt
eval "$(starship init bash)"

# aliases
alias ls='exa -al --color=always --group-directories-first'
alias clean='rm -rf ~/.cache/*'
alias ga='git add .'
alias gc='git commit -m "backing up"'
alias gp='git push'
alias gs='git status'
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias backup="clean && ga && gc && gp"
alias mi='~/.config/micro/micro'

# exports
export TERM=xterm-256color
export EDITOR=nvim

