" indentation
filetype indent on
set autoindent
set expandtab 
set smarttab
set shiftwidth=4
set tabstop=4

" line and cursor settings
set number relativenumber
set ruler
set cursorline
set scrolloff=4

" search
set ignorecase
set hlsearch
set incsearch
set smartcase

" mouse support
set mouse=a

" status line
set noshowmode
set laststatus=2

" encoding
set encoding=utf-8

" hidden buffers
set hidden

" systemwide clipboard
set clipboard=unnamedplus

" no backup/swap
set noswapfile
set nobackup

" vim-plug
call plug#begin('~/.config/nvim/vim-plug')

Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'morhetz/gruvbox'
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'lilydjwg/colorizer'

call plug#end()

" colors
colorscheme dracula
set background=dark
set termguicolors
syntax enable

let g:lightline = {
      \ 'colorscheme': 'dracula',
      \ }
