[options]
allow_bold = true
audible_bell = false
bold_is_bright = false
cell_height_scale = 1.0
cell_width_scale = 1.0
clickable_url = true
dynamic_title = true
font = Roboto Mono 11
fullscreen = true
icon_name = termite
mouse_autohide = false
scroll_on_output = false
scroll_on_keystroke = true
scrollback_lines = 10000
search_wrap = true
urgent_on_bell = true
hyperlinks = false

# $BROWSER is used by default if set, with xdg-open as a fallback
browser = xdg-open

# "system", "on" or "off"
cursor_blink = off

# "block", "underline" or "ibeam"
cursor_shape = block

# Hide links that are no longer valid in url select overlay mode
filter_unmatched_urls = true

# Emit escape sequences for extra modified keys
modify_other_keys = false

# set size hints for the window
size_hints = false

# "off", "left" or "right"
scrollbar = off

[colors]
# If both of these are unset, cursor falls back to the foreground color,
# and cursor_foreground falls back to the background color.
cursor = #f8f8f2
cursor_foreground = #282a36

foreground = #f8f8f2
foreground_bold = #f8f8f2
background = #282a36

# 20% background transparency (requires a compositor)
#background = rgba(63, 63, 63, 0.8)

# If unset, will reverse foreground and background
highlight = #44475a

# Colors from color0 to color254 can be set
color0 = #000000
color1 = #ff5555
color2 = #50fa7b
color3 = #f1fa8c
color4 = #bd93f9
color5 = #ff79c6
color6 = #8be9fd
color7 = #bfbfbf
color8 = #4d4d4d
color9 = #ff6e67
color10 = #5af78e
color11 = #f4f99d
color12 = #caa9fa
color13 = #ff92d0
color14 = #9aedfe
color15 = #e6e6e6

[hints]
font = Monospace 12
foreground = #dcdccc
background = #3f3f3f
active_foreground = #e68080
active_background = #3f3f3f
padding = 2
border = #3f3f3f
border_width = 0.5
roundness = 2.0

# vim: ft=dosini cms=#%s
