#include <iostream>
#include <ostream>
#include <cmath>

using namespace std;

int pow(int n, int m) {
    int result = 1;
    for(int i = 0; i < m; i++) {
        result = result * n;
    }
    return result;
}

int main() {

    double num1, num2, result;
    string op;

    cout << endl << "enter first num: ";
    cin >> num1;
    cout << "enter op: ";
    cin >> op;
    cout << "enter second num: ";
    cin >> num2;

    if(op == "+") {
        result = num1 + num2;
    }
    else if(op == "-") {
        result = num1 - num2;
    }
    else if(op == "*") {
        result = num1 * num2;
    }
    else if(op == "/") {
        result = num1 / num2;
    }
    else if(op == "^") {
        result = pow(num1, num2);
    }
    cout << "result: " << result << endl;

    return 0;
}
