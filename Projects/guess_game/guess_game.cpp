#include <iostream>

using namespace std;

int main() {

    int secNum = 7;
    int guess;
    int guessCount = 0;
    int guessLimit = 3;
    bool outOfGuesses = false;

    while(secNum != guess and !outOfGuesses) {
        
        if(guessCount < guessLimit) {
            cout << "enter guess: ";
            cin >> guess;
            guessCount ++;
        } else {
            outOfGuesses = true;
        }
    }
    if(outOfGuesses) {
        cout << "you lose";
    } else {
        cout << "you win";
    }

    return 0;
}
