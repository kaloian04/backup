#include <iostream>
using namespace std;

int pow(int n, int m) {
    int result = 1;
    for(int i = 0; i < m; i++) {
        result = result * n;
    }
    return result;
}

int main() {
    int a, b;
    cin >> a >> b;
    cout << pow(a, b);

    return 0;
}


